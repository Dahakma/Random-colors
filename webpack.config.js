const path = require('path');


module.exports = {
	//context     : path.resolve(__dirname, './src'),
	entry: "./src/index.js",
	output: {
		filename: 'color.js',
		path: path.resolve(__dirname, "dist"),
		library: "rc",   // Important
		libraryTarget: 'umd',   // Important
		
	},

/*
	optimization: {
        minimize: false //REMOVE FOR PRODUCTION!!
    },
*/




	/*
	output: {
		filename: 'color.js',
		path: path.resolve(__dirname, "dist"),
		library: "myLibrary",   // Important
		libraryTarget: 'umd',   // Important
		//umdNamedDefine: true   // Important
	},
	*/
	/* 
	devServer   : {
        contentBase: './public',
        openPage   : 'index.html'
    },
 *//*
	module: {
      rules: [
         {
            test: /\.js$/,
            include: path.resolve(__dirname, 'src'),
            loader: 'babel-loader',
            query: {
               presets: ['env']
            }
         }
      ]
   },	*/
	module      : {
        rules: [
            {
                test   : /\.js$/,
                include: [/src/],
                use    : {
                    loader : 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
        ]
    },
	devServer: {
		open: true, // Tells webpack-dev-server to open your default browser
		watchContentBase: true,
		contentBase: path.join(__dirname, "dist"),
		
		//hot: true,
		injectClient: false, //see https://github.com/webpack/webpack-dev-server/issues/2484
		/*watchOptions: {
            poll: true
        },
		*/
		
		//openPage   : 'index.html',
		//port: 9000
		//watchContentBase: true
	}
  
}