/**
	returns object with {h: hue, s: saturation, l: lightness, a: alpha}
	input string with hsla color
*/
export const parseHSLA = function(input){
	/*
		regex:
		\d*[.])?\d* - handles float numbers (for integers would be \d+ enough)
		
		/hsl[a]?\(
			\s* (\d+[.]?\d*) \s* ,
			\s* (\d+[.]?\d*) % \s* ,
			\s* (\d+[.]?\d*) % \s* [,]?
			\s* (\d+[.]?\d*)? \s* 
		\)/
	*/

	const tempco = /hsl[a]?\(\s*(\d+[.]?\d*)\s*,\s*(\d+[.]?\d*)%\s*,\s*(\d+[.]?\d*)%\s*[,]?\s*(\d+[.]?\d*)?\s*\)/.exec(input);	
	
	if(tempco){
		tempco[4] ??= 1; //default alpha (in case of just HSL)
		return {
			h: +tempco[1],
			s: +tempco[2],
			l: +tempco[3],
			a: +tempco[4],
		};
	}else{
		console.warn(`parseHSLA - incorrect input (${input})`);
		return null;
	}
};

export const parseHSL = parseHSLA;


/**
	returns string with HSLA color
	input either object  {h: hue, s: saturation, l: lightness, a: alpha}
	or arguments (hue, saturation, lightness, alpha)
	or object {hue: hue, saturation: saturation, lightness: lightness, alpha: alpha}
*/
	
export	function stringifyHSLA(input){
	let tempco;
	if(typeof input !== "object" && typeof arguments[1] !== "undefined"){
		tempco = {
			h: arguments[0],
			s: arguments[1],
			l: arguments[2],
			a: arguments[3] ?? 1,
		};
	}else if(typeof input === "object" && (typeof input.h !== undefined || typeof input.hue !== undefined) ){
		tempco = {
			h: input.h ?? input.hue,
			s: input.s ?? input.saturation ?? input.satur, //TODO SATURATION
			l: input.l ?? input.lightness ?? input.light,
			a: input.a ?? input.alpha ?? 1,
		};
	}
	
	if( !tempco || typeof tempco.h === undefined || typeof tempco.a === undefined || typeof tempco.l === undefined){
		console.warn(`stringifyHSLA - incorrect input (${input})`)
	}
	
	tempco.h = Math.round(tempco.h);
	tempco.s = Math.round(tempco.s);
	tempco.l = Math.round(tempco.l);
	tempco.a = Math.round(tempco.a * 1000) / 1000;
	
	return `hsla(${tempco.h}, ${tempco.s}%, ${tempco.l}%, ${tempco.a})`;
}


/**
	returns object with {r: red, g: green, b: blue, a: alpha}; //e.g. {r: 60, g: 179, b: 113, a: 1}
	input string with RGB or RGBA color //e.g. "rgb(60, 179, 113)"
*/
export const parseRGB = function(input){
	//const tempco = /rgb\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)/.exec(input);	
	/*
		/rgb[a]?\(
			\s*(\d+)\s* ,
			\s*(\d+)\s* ,
			\s*(\d+)\s* [,]?
			\s*(\d+[.]?\d*)?\s*
		\)/
	
	*/
	
	const tempco = /rgb[a]?\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*[,]?\s*(\d+[.]?\d*)?\s*\)/.exec(input);
	
	if(tempco){
		tempco[4] ??= 1; //default alpha (in case of just HSL)
		return {
			r: parseInt(tempco[1]),
			g: parseInt(tempco[2]),
			b: parseInt(tempco[3]),
			a: +tempco[4],
		}
	}else{
		console.warn(`parseRGB - incorrect input (${input})`)
		return null;
	}
};	

export const parseRGBA = parseRGB;


/**
	returns string with RGB color //e.g. "rgb(60, 179, 113)"
	input either object  {r: red, g: green, b: blue} //e.g. {r: 60, g: 179, b: 113}
	or arguments (red, green, blue)
	or object  {red: red, green: green, blue: blue}
*/
	
export	function stringifyRGB(input){
	let tempco;
	//TODO - input is array
	if(typeof input !== "object" && typeof arguments[1] !== "undefined"){
		tempco = {
			r: arguments[0],
			g: arguments[1],
			b: arguments[2],
			a: arguments[3],
		};
	}else if(typeof input === "object" && (typeof input.r !== undefined || typeof input.red !== undefined) ){
		tempco = {
			r: input.r ?? input.red,
			g: input.g ?? input.green,
			b: input.b ?? input.blue,
			a: input.a,
		};
	}

	if( !tempco || typeof tempco.r === undefined || typeof tempco.g === undefined || typeof tempco.b === undefined){
		console.warn(`stringifyRGB - incorrect input (${input})`)
	}
	
	//TODO - TO INTEGER
	
	if(tempco.a === undefined || tempco.a === 1){ //TODO isNaN?
		return `rgb(${tempco.r}, ${tempco.g}, ${tempco.b})`;
	}else{
		return `rgba(${tempco.r}, ${tempco.g}, ${tempco.b}, ${tempco.a})`;
	}
};