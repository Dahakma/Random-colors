import {nameList} from "./list";
import {HEXtoHSLA} from "./convert";
import {color} from "./config";
import {parseHSLA} from "./parse";

export function colorByName(name){
	for(const key in nameList){
		for(let i = 0; i < nameList[key].length; i++){
			if(nameList[key][i][1] == name ){
				return HEXtoHSLA( nameList[key][i][0] );
			}
		}
	}
	console.warn(`The ${color} "${name}" was not found in the list of ${color}s. `);
	return null;
}

function colorDifference(inputHSL, testedHSL){
	/*
	const secondRGB = HEX2RGB( nameList[shade[k]][i][0] );
	
	const redDiff = firstRGB.r - secondRGB.r;
	const grnDiff = firstRGB.g - secondRGB.g;
	const bluDiff = firstRGB.b - secondRGB.b;

	const diffRGB = Math.sqrt( Math.pow(redDiff,2) + Math.pow(grnDiff,2) + Math.pow(bluDiff,2) );
	*/
	
	const hueDiff = inputHSL.h - testedHSL.h;
	const satDiff = inputHSL.s - testedHSL.s;
	const ligDiff = inputHSL.l - testedHSL.l;

	const diffHSL = Math.sqrt( Math.pow(hueDiff,2) + Math.pow(satDiff,2) + Math.pow(ligDiff,2) );
	
	return diffHSL; //(diffHSL + diffRGB) / 2
}


/*
//TODO - do not delete but probably remove from here; used only for experiments
function lowestPossibleDifference(inputHSL){
	let array = [];
	for(const key of Object.keys(nameList) ){
		for(let i = 0; i < nameList[key].length; i++){
			for(const key2 of Object.keys(nameList) ){
				for(let i2 = 0; i2 < nameList[key2].length; i2++){
					array.push(
						[
							colorDifference( HEXtoHSLA( nameList[key][i][0] ),  HEXtoHSLA( nameList[key2][i2][0] )  ),
							nameList[key][i][1]+" > "+nameList[key2][i2][1]
						]
					)
				}
			}
		}
	}
	array = array.filter( a => a[0] );
	array = array.sort( (a,b) => a[0] - b[0] ); 
	console.log(`tl;dr the smalles difference is ${array[0][1]} - ${array[0][0]} `);
	console.log(`the biggest difference is ${array[array.length-1][1]} - ${array[array.length-1][0]} `);
	return array;
}	
*/

export function nameColor(input){
	//TOCO - checking of input 
	if(typeof input === "string"){
		input = parseHSLA(input);
	}
	
	let shadeArray;
	
	if(input.h < 60){
		shadeArray = ["red","yellow"];
	}else if(input.h < 120){
		shadeArray = ["yellow","green"];
	}else if(input.h < 180){
		shadeArray = ["green","cyan"];
	}else if(input.h < 240){
		shadeArray = ["cyan","blue"];
	}else if(input.h < 300){
		shadeArray = ["blue","pink"];
	}else{
		shadeArray = ["pink","red"];
	};

			
	if(input.l < 25){
		shadeArray.push("black");
	}else if(input.l > 75){
		shadeArray.push("white");
	}else if(input.s < 50){
		shadeArray.push("gray");
	};
	

	function findColor(shadeArray){
		const inputHSL = input;
		const result = {
			name: "",
			difference: Infinity,
		}
		

		
		for(const shade of shadeArray){
			for(let i = 0; i < nameList[shade].length; i++){
				const difference = colorDifference(inputHSL,  HEXtoHSLA( nameList[shade][i][0] )  )
				
				if(difference < result.difference){
					result.difference = difference;
					result.name = nameList[shade][i][1]
				}
				
				if(difference < 0.46){ //the smalles difference between colors is 0.470 (yellow and lemon); could be interupted immediatelly
					return result; 
				}
			};
		};
		
		return result;
	};

	const output = findColor(shadeArray);
	return output.name;	
}