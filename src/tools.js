import {nameList} from "./list";
import {HEXtoHSLA, HEXtoRGB} from "./convert";
import {color} from "./config";
import {stringifyHSLA, stringifyRGB} from "./parse";


function container(content){	//TODO MESSY AND PROVISIONAL
	const div = document.createElement("div");
	div.id = "cnt";
	div.innerHTML = content;
	document.body.appendChild(div);  
	
	const exit = document.createElement("div");
	exit.innerHTML = `EXIT`;
	exit.style.cursor = "pointer";
	//exit.style.fontWeight: "bold"; //TODO - doesnt work for some reason
	
	exit.addEventListener("click", ()=> document.getElementById("cnt").remove() );
	div.appendChild(exit);  
	
	
	const exitB = document.createElement("div");
	exitB.innerHTML = `EXIT`;
	exitB.style.cursor = `pointer`;
	exitB.addEventListener("click", ()=> document.getElementById("cnt").remove() );
	div.insertBefore(exitB, div.firstChild);
	
}

export function listColors(input){
	let tab = `<table>
		 <tr>
			<th></th>
			<th>Name</th>
			<th>HEX</th>
			<th>RGB</th>
			<th>HSLA</th>
		</tr>
	
	`;
	
	for(const key in nameList){
		if(input && key !== input){
			continue;
		}
		
		for(let i = 0; i < nameList[key].length; i++){
			tab += `<tr>
				<td>
					<div style = "background: ${nameList[key][i][0]}; height: 1em; width: 2em;">
				</td>
				<td>
					${nameList[key][i][1]}
				</td>
				<td>
					${nameList[key][i][0]}
				</td>
				<td>
					${stringifyRGB( HEXtoRGB(nameList[key][i][0]) )}
				</td>
				<td>
					${ stringifyHSLA( HEXtoHSLA(nameList[key][i][0]) ) }
				</td>
				<td>
					<div style = "background: ${nameList[key][i][0]}; height: 1em; width: 2em;">
				</td>
			</tr>`;
		}
	}
	
	tab += `</table>`;
	container(tab);
}