import {stringifyHSLA} from "./parse";

const colorList = {
	all: [[0,360],[0,100],[0,100]],
	random: [[0,360],[50,100],[25,75]],
	mono: [[0,0],[0,0],[5,95]],
	
	red: [[0,10],[75,100],[35,65]],
	yellow: [[50,70],[75,100],[35,65]],
	green: [[110,130],[75,100],[35,65]],
	cyan: [[170,190],[75,100],[35,65]],
	blue: [[230,250],[75,100],[35,65]],
	violet: [[280,320],[75,100],[35,65]],
	
	
	
	
	
	//ALL - mostly for testing
	//	all: [[0,360],[0,100],[0,100]],
		//POLY - all random but distinctive colors
	//	poly: [[0,360],[50,100],[25,75]],
	//	random: [[0,360],[50,100],[25,75]],
		//MONO - black and white
	//	mono: [[0,0],[0,0],[5,95]],
		//GRAY
		gray: [[0,0],[0,0],[15,85]],
		grey: [[0,0],[0,0],[15,85]],
		//BLACK - but not total black
		black: [[0,360],[5,15],[5,15]],
		
		//HAIR COLORS 
		blonde: [[44,48],[60,100],[50,80]],
		platinum: [[44,48],[0,70],[80,90]],
		
		pinkhead: [[330,340],[60,90],[40,70]],
		greenhead: [[101,105],[60,90],[40,60]],
		bluehead: [[220,225],[60,100],[40,60]],
		purplehead: [[290,310],[80,100],[23,36]],
		
		redhead: [[3,7],[70,95],[30,53]],
		
		auburn: [[0,10],[70,100],[18,24]],
		
		raven: [[0,0],[0,0],[8,15]],
		brunette: [[15,25],[40,60],[20,40]],		
			
		//BRIGHT - all distinctive bright colors with high saturation
		bright: [[0,360],[85,100],[40,60]],	
		
		//MANLYBRIGHT - bright but without pink shades
		manlyBright: [[0,250],[85,100],[40,60]],	
		
		//JEANS - light to dark blue shades suitable for jeans
		jeans: [[200,250],[40,100],[15,50]],
		
		//YOGA - darkers shades with lower saturation suitable for sweatpants 
		yoga: [[0,360],[20,60],[15,40]],

		barbie: [[295,335],[75,100],[40,60]],
		//yellow metal - something between gold, bronze, copper and brass
//		metal: [[45,55],[75,95],[44,54]],
		metal: [[45,65],[1,4],[40,60]],
		
		gold: [[48,52],[80,100],[48,52]],
		bronze: [[38,48],[75,100],[48,52]],
		silver: [[45,55],[0,10],[70,80]],
		steel: [[200,210],[20,40],[45,65]],
		
		
		light: [[0,360],[25,45],[70,15]],
		
		
		
		
}




let SEED;

/*
export function distribution(input){
	let result = [0,0,0,0,0,0,0,0,0,0,0,0,-1];
	for(let i = 0; i < input; i++){
		const a = randomColor("all").l;
		if(a < 25){
			result[0]++;
		}else if(a < 50){
			result[1]++;
		}else if(a < 75){
			result[2]++;
		}else if(a < 100){
			result[3]++;
		}	
			 
		if(a < 30){
			result[0]++;
		}else if(a < 60){
			result[1]++;
		}else if(a < 90){
			result[2]++;
		}else if(a < 120){
			result[3]++;
		
		}else if(a < 150){
			result[4]++;
		}else if(a < 180){
			result[5]++;
		}else if(a < 210){
			result[6]++;
		}else if(a < 240){
			result[7]++;
		}else if(a < 270){
			result[8]++;
		}else if(a < 300){
			result[9]++;
		}else if(a < 330){
			result[10]++;
		}else{
			result[11]++;
		};
		 
	}
	
	return result;
}
*/
	
function random(min = 0, max = 1){
	//see: http://indiegamr.com/generate-repeatable-random-numbers-in-js/ 
	SEED = (SEED * 9301 + 49297) % 233280;
	let random = SEED / 233280;
	return min + random * (max - min);
}
	
export function randomColor(shade, seed){
	SEED = seed ?? Math.random() * 12345;
	//console.log(SEED);
	let source = colorList[shade];
	source ??= colorList.random;
	
	function randomValue(index){
		return Math.round(  random( source[index][0], source[index][1] )  );
	}
	
	return {
		h: randomValue(0),
		s: randomValue(1),
		l: randomValue(2),
		a: source[3] ?   Math.round(  random( source[3][0], source[3][1] )  * 1000   )   /   1000    :    1,
	};
}


export function stringifyRandomColor(shade, seed){
	return stringifyHSLA( randomColor(shade, seed) );
}
