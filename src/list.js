import {gray} from "./config"

console.log(gray);

export const nameList = {
	//white
	white: [
		["#FFFFFF","white"],
		["#C0C0C0","silver"],
		
		["#FFFFF0","ivory"],
		["#FFF5EE","seashell"],
		["#EDEAE0","alabaster"],
		["#F0EAD6","eggshell"],
		["#F7E7CE","champagne"],
		["#FFFDD0","cream"],
		["#E5E4E2","platinum"],

		["#D3D3D3","light "+gray],
	],
	
	//black
	black: [
		//["#808080",gray],
		//["#808080","dim "+gray],
		//["#555D50","ebony"],

		["#1A1110","licorice"],
		["#353839","onyx"],
		//["#555D50","ebony"],
		
		//["#36454F","charcoal"],
		["#242124","raisin black"],
		["#343434","jet"],
		
		
		

	],
	
	//grey
	gray: [
		["#D3D3D3","light "+gray],
		["#808080",gray],
		["#808080","dim "+gray],
		["#555D50","ebony"],
		["#36454F","charcoal"],
		
	],
	
	//red - 0
	red: [
		["#FF0000","red"], //0 !!!
		["#800000","maroon"], //0 25
		
		["#960018","carmine"],
		["#DC143C","crimson"],	
		["#8B0000","dark red"],	
		
		["#FF2400","scarlet"],
		
	//	["#800020","madder"],	
		["#A50021","madder"],	
		["#E52B50","amaranth"],
		["#FFC0CB","pink"],
		["#FA8072","salmon"],
		["#800015","burgundy"],
		["#FE6F5E","bittersweet"],
		["#FF7F50","coral"],			
		["#E34234","vermilion"],
		["#E44D2E","cinnabar"],	
		["#BC3F4A","sanguine"],
		["#C04000","mahogany"],
		["#A45A52","redwood"],	
		["#E0115F","ruby"],
		["#FF6347","tomato"],
		["#D05340","jasper"],
		
		
		//brown
		["#635147","umber"],
		["#32001B","blackberry"],
		["#59260B","seal brown"],
		["#9F1D35","vivid burgundy"],
		["#43302E","dark burgundy"], //old burgundy
		
	],
	
	//yellow - h60
	yellow: [
	 
		["#FFFF00","yellow"], //60 !!!
		["#808000","olive"],
		
		["#FFFFE0","light yellow"],
		["#FFFFCC","cream"],
		["#FFFACD","lemon chiffon"],
		["#FDFF00","lemon"],
		["#DAA520","goldenrod"],
		["#FFAA1D","bright yellow"],
		["#F8DE7E","jasmine"],
		["#EEDC82","flax"],
		["#FFBF00","amber"],

		//chartreuse
		["#EEEA62","greenish yellow"],
		["#D1E231","pear"],
		
		//orange
		["#FF7F00","orange"], //color wheel	//30 !! 
		["#FFA500","orange"], //web
		["#FF8C00","dark orange"], 
		["#FED8B1","light orange"], 
		["#FFE5B4","peach"], 
		["#F28500","tangerine"], 
		["#FDBCB4","melon"],
		["#C06000","tawny"],
		["#F4C430","saffron"],
		["#FF7518","pumpkin"],
		["#FBCEB1","apricot"], 
		["#F88379","tea rose"], 
		["#ED9121","carrot orange"], 
		["#FFD700","golden"],

		//brown
		["#804000","brown"], //30 25
		//["#964B00","brown"], 
		["#F5F5DC","beige"],
		["#954535","chestnut"],
		["#5C4033","dark brown"],
		["#C3B091","khaki"],
		["#A52A2A","red-brown"], //web
		["#CC7722","ochre"],
		["#B87333","copper"],
		["#882D17","sienna"],
		["#CD7F32","bronze"],
		["#BC8F8F","rosy brown"],
		["#F4A460","sandy brown"],
		["#F0DC82","buff"],
		["#7B3F00","chocolate"],
		["#80461B","russet"],
		["#D2B48C","tan"],
		["#483C32","taupe"],
		["#5C5248","walnut brown"],
		["#C19A6B","wood brown"],
		["#E5AA70","fawn"],
		
	],
	
	//green - l h120
	green: [
		["#00FF00","lime"],
		["#008000","green"],
		
		//chartreuse
		["#568203","avocado"],
		["#8DB600","apple green"],
		["#9ACD32","yellow-green"],
		["#93C572","pistachio"],
		["#A7FC00","spring bud"],
		["#ADFF2F","green-yellow"],
 
		["#7FFF00","chartreuse"], // 90!!!
		["#00FF7F","spring green"], // 150!!!
			 
		["#355E3B","hunter green"],
		["#4D5D53","feldgrau"],
		["#00693E","dartmouth green"],
		["#ACE1AF","celadon"],
		["#006A4E","bottle green"],
		["#4B5320","army green"],
		["#39FF14","neon green"],
		["#ADFF2F","green-yellow"],
		["#03C03C","dark pastel green"],
		["#013220","dark green"],
		["#4FFFB0","bright mint"],
		["#66FF00","bright green"],
		["#1CAC78","green"],
		["#98FB98","pale green"],
		["#32CD32","lime green"],
		 ["#90EE90","light green"],
		["#006400","dark green"],
		["#2E8B57","sea green"],
		["#0BDA51","malachite"],
		["#00A86B","jade"],
		["#50C878","emerald"],
	//	["#008080","teal"],
		["#D0F0C0","tea green"],
		["#009E60","shamrock green"],
		["#01796F","pine green"],
		["#317873","myrtle green"],
		["#8A9A5B","moss green"],
		["#4A5D23","dark moss green"],
		["#3EB489","mint"],
		["#74C365","mantis"],
		["#A9BA9D","laurel green"],
		["#29AB87","jungle green"],
		["#228B22","forest green"],
		["#71BC78","fern"],
		["#4F7942","fern green"],
		["#8F9779","artichoke"],
		["#87A96B","asparagus"],

	],
	
	//cyan - h180
	cyan: [
		["#00FFFF","cyan"], // 180!!!
		["#008080","teal"],
		
		["#7FFFD4","aquamarine"],
		["#40E0D0","turquoise"],

		["#0D98BA","blue-green"],
		["#B2FFFF","celeste"],
		["#007BA7","cerulean"],
		["#008B8B","dark cyan"],
		["#7DF9FF","electric blue"],
		["#1164B4","green-blue"],
		["#E0FFFF","light cyan"],
		["#20B2AA","light sea green"],
		["#87D3F8","pale cyan"],
		["#80DAEB","sky blue"],
		["#43B3AE","verdigris"],
		["#40826D","viridian"],

		["#5E716A",gray+"-green"],
	],	
	
	//blue - 240
	blue: [
		["#0000FF","blue"],
		["#000080","navy"],
		
		["#4000FF","ultramarine"],

		["#ADD8E6","light blue"],
		["#CCCCFF","periwinkle"],
		["#99FFFF","ice blue"],
		["#8DA399","morning blue"],
		["#1F75FE","blue"], //crayola
		["#0093AF","blue"], 
		["#6CB4EE","argentinian blue"],
		["#4D4DFF","neon blue"],
		["#00008B","dark blue"],
		["#191970","midnight blue"],
		["#082567","sapphire"],
		["#15F4EE","fluorescent blue"],
		["#004080","cobalt blue"],//210 25
		["#007FFF","azure"], //210 !!!
		["#003153","prussian blue"],
		
		//gray
		["#4C5866","marengo"],
		["#708090","slate gray"],
		["#6699CC","blue-gray"],
		["#91A3B0","cadet gray"],
		["#9090C0","cool gray"],

	],
	
	//pink 300
	pink: [
		["#FF00FF","magenta"], //300 !!
		//["#FF00FF","fuchsia"],
		["#7F007F","purple"], //300 25!!!
		//["#800080","purple"],
			
		//magenta
		["#AB274F","amaranth purple"],
		["#FF1DCE","hot magenta"],
		["#FC0FC0","shocking pink"],
		["#DA70D6","orchid"],
		["#843179","plum"],
		["#8B008B","dark magenta"],
		
		//violet
		["#7F00FF","violet"], // 270!!

		["#6a0080","dark violet"], //270 25
		["#E0B0FF","mauve"],
		["#B57EDC","lavender"],
		["#C9A0DC","wisteria"],
		["#645394","ultra violet"],
		["#6F2DA8","grape"],

		//pink
		["#FFC0CB","pink"],
		["#FFB6C1","light pink"],
		["#FF69B4","hot pink"],
		["#FF1493","deep pink"],
		["#FDDDE6","piggy pink"],
		["#F9CCCA","pale pink"],
		["#F2BDCD","orchid pink"],
		["#F2C1D1","fairy tale"],
		["#FBAED2","lavender pink"],
		["#FFBCD9","cotton candy"],
		["#FFA6C9","carnation pink"],
		["#C4AEAD","silver pink"],
		["#DA1884","barbie pink"],
		["#DE5285","fandango pink"],
		["#E63E62","paradise pink"],
		["#FB607F","brink pink"],
		//["#FF007F","bright pink"], //rose?
		["#FF66CC","rose pink"],
		["#FC0FC0","shocking pink"],
		["#FF6FFF","ultra pink"],

		//rose
		["#FF007F","rose"],
		["#FFE4E1","misty rose"],
		["#FF66CC","rose pink"],
		["#65000B","rosewood"],
		["#905D5D","rose taupe"],
		["#B3446C","raspberry rose"],
		["#C21E56","rose red"],
		["#FB607F","brink pink"],

	],
	
	main: [
		["#00FF00","lime"],
		["#008000","green"],
		
		["#0000FF","blue"],
		["#000080","navy"],
		
		["#008080","teal"],
		["#00FFFF","cyan"],
		["#007FFF","azure"],
		
		["#808000","olive"], //60 25
	//	["#F5F5DC","beige"],
		["#964B00","brown"], 			
		["#FF7F00","orange"],
		["#FFFF00","yellow"],
		
		["#FF0000","red"],
		["#800000","maroon"],
		
		["#FFC0CB","pink"],
		["#7F00FF","violet"],
		["#FF00FF","magenta"],
		["#800080","purple"],
		
		["#000000","black"],
		["#FFFFFF","white"],
		
		["#D3D3D3","light "+gray],
		["#808080",gray],
		["#808080","dim "+gray],
	],
};