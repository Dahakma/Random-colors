# Random colors

This is a library of various utility functions which should simplify working with web colors. 

The building of this library had three main purposes: 

- I prefer to use HSLA (hue-saturation-lightness-alpha) colors because it is easier to visualise in your head  `hsla(100, 100%, 50%, 1)` than `#5f0`. However, sometimes it is better or necessary to use different colors (e.g. `<input type="color">` uses hexadecimal codes).
- I am working on games and silly stuff which often involve a lot of procedural generation including generation of colors
- all other existing libraries seemed too cumbersome and I wanted something simpler exactly fitting my needs

It is still a work in a progress. 


## Credits

- the naming of colors is inspired by project [Name that Color](https://chir.ag/projects/ntc/) by Chirag Mehta however I both replaced the whole database of colors and significantly tweaked the algorithm 
- some conversion algorithms are stolen from [Michael Jackson](https://gist.github.com/mjackson/5311256)


## Documentation
* [Name](#name)
	* [nameColor](#namecolor)
	* [colorByName](#colorbyname)
	* [listColors](#listcolors)
* [Random](#random)
	* [randomColor](#randomcolor)
	* [stringifyRandomColor](#stringifyRandomColor)
* [Parsing](#parsing)
	* [parseHSLA](#parsehsla)
	* [stringifyHSLA](#stringifyhsla)
	* [parseRGB](#parsergb)
	* [stringifyRGB](#stringifyrgb)
* [Conversions](#conversions)
	* [RGBtoHSLA](#rgbtohsla)
	* [HEXtoRGB](#hextorgb)
	* [HEXtoHSLA](#hextohsla)
	* [HEXtoHSLA](#hextohsla)
	* [HSLAtoRGB](#hslatorgb)
	* [RGBtoHEX](#rgbtohex)
	* [HSLAtoHEX](#hslatohex)


## Name

## nameColor
- finds the closest color to the inputted one and returns its name
- input: object `{h: hue, s: saturation, l: lightness, a: alpha}`
	- or string with HSLA color
- returns: string with the name of the color

```javascript
rc.name({h: 309, s: 100, l: 35});
//"dark magenta"
```
```javascript
rc.name("hsla(39, 100%, 50%, 1)");
//"orange"
```

[🔝](#documentation)


## colorByName
- looks for the color in the list of colors and returns its value
- input: string with the name of the color
- returns: object `{h: hue, s: saturation, l: lightness, a: alpha}` or null if not found

```javascript
rc.colorByName("mauve");
//{h: 276.4556962025316, s: 100, l: 84.50980392156863, a: 1};
```
```javascript
rc.stringifyHSLA( rc.colorByName("dartmouth green") );
//"hsla(155, 100%, 21%, 1)"
```

[🔝](#documentation)


## listColors
- displays table with the whole list of colors

```javascript
rc.listColors();
```

[🔝](#documentation)


## Random

## randomColor
- returns a random color
- `rc.randomColor(shade, seed)`
- input: 
	- shade - string with more specific desired color; optional;
		- `mono` (monochromatic, i.e. black and white)
		- `red`
		- `yellow`
		- `green`
		- `cyan`
		- `blue`
		- `violet`
	- seed - number used for randomisation; optional; (the same shade and number should always returns the same color)
- returns: object `{h: hue, s: saturation, l: lightness, a: alpha}`

```javascript
rc.randomColor()
//{h: 5, s: 83, l: 51, a: 1}
```
```javascript
rc.randomColor("red", 666);
//{h: 8, s: 86, l: 46, a: 1}
```

## stringifyRandomColor

- the same as above but returns HSLA string 

```javascript
rc.stringifyRandomColor("blue");
//"hsla(248, 95%, 51%, 1)"
```

[🔝](#documentation)


## Parsing
- the library generally operates with HSLA colors parsed into objects `{h: hue, s: saturation, l: lightness, a: alpha}` which are easier to edit
- strings could be simply parsed by `parseHSLA` or turned back into strings usable in CSS or anywhere else by `stringifyHSLA`

### parseHSLA
- converts a string with HSLA color into an object (beware only saturation and lightness are percentual values)
- input: string with HSLA color (HSL works too, default *alfa* will be `1`)
- returnS: object `{h: hue, s: saturation, l: lightness, a: alpha}`

```javascript
rc.parseHSLA("hsla(200, 50%, 44%, 0.5)")
//{h: 200, s: 50, l: 44, a: 0.5}
```

[🔝](#documentation)


### stringifyHSLA
- converts values of HSLA color into string
- input:
	- object `{h: hue, s: saturation, l: lightness, a: alpha}`
	- or arguments `(hue, saturation, lightness, alpha)`
	- or object `{hue: hue, saturation: saturation, lightness: lightness, alpha: alpha}`
	- (*alfa* could be omitted, default `1`)
- output: string

```javascript
rc.stringifyHSLA({h: 100, s: 50, l : 50, a: 1});
//"hsla(100, 50%, 50%, 1)"
```

[🔝](#documentation)


### parseRGB
- converts string with RGB color into object
- input: string with RGB or RGBA color (default *alfa* is 1)
- returns: object `{r: red, g: green, b: blue, a: alfa};`

```javascript
rc.parseRGB("rgb(255, 26, 251)")
//{r: 255, g: 26, b: 251, a: 1}
```

[🔝](#documentation)


### stringifyRGB
- converts values of RGB color into string
- input:
	- object `{r: red, g: green, b: blue}`
	- or arguments `(red, green, blue)`
- returns: string with RGB color
	- in the case the alfa is defined and has other value than default 1 return RGBA color

```javascript
rc.stringifyRGB({r: 255, g: 26, b: 251});
//"rgb(255, 26, 251)"
```
```javascript
rc.stringifyRGB({r: 255, g: 26, b: 251, a: 0.5});
//"rgba(255, 26, 251, 0.5)"
```

[🔝](#documentation)


## Conversions

| to | HSLA | HEX | RGB | 
|:--- |:---:|:---:|:---:|
| HSLA| x | [HSLAtoHEX](#hslatohex) | [HSLAtoRGB](#hslatorgb) |
| HEX | [HEXtoHSLA](#hextohsla) | x | [HEXtoRGB](#hextorgb) |
| RGB | [RGBtoHSLA](#rgbtohsla) | [RGBtoHEX](#rgbtohex) | x |


### RGBtoHSLA
- converts RGB color into HSLA
- input: object with `{r: red, g: green, b: blue}` or `{r: red, g: green, b: blue, a: alpha}`
	- or string with RGB or RGBA color 
- string with RGB or RGBA color (default *alfa* is 1)
- returns: object `{h: hue, s: saturation, l: lightness, a: alpha}`

```javascript
rc.RGBtoHSLA({r: 255, g: 26, b: 251})
//{h: 301.04803493449776, s: 100, l: 55.09803921568628, a: 1}
```
```javascript
rc.stringifyHSLA( rc.RGBtoHSLA("rgba(8, 108, 221, 1)") );
//"hsla(212, 93%, 45%, 1)"
```

[🔝](#documentation)


### HEXtoRGB
- converts hexadecimal color into RGB
- input: string with hexadecimal color (works even without the initial "#")
- returns: object `{r: red, g: green, b: blue}`

```javascript
rc.HEXtoRGB("#da0bb4")
//{r: 218, g: 11, b: 180}
```

[🔝](#documentation)


### HEXtoHSLA
- converts hexadecimal color into HSLA
- input: string with hexadecimal color (works even without the initial "#")
- returns: object `{h: hue, s: saturation, l: lightness, a: alpha}`

```javascript
rc.HEXtoHSLA("da0bb4")
//{h: 311.0144927536232, s: 90.39301310043669, l: 44.90196078431372, a: 1}
```

[🔝](#documentation)


### HEXtoHSLA
- converts HSLA color to RGB
- input: HSLA object `{h: hue, s: saturation, l: lightness, a: alpha}`
	- or string with HSLA color 
- returns: object `{r: red, g: green, b: blue, a: alpha}`

```javascript
rc.HSLAtoRGB({h: 200, s: 50, l: 44, a: 0.5})
//{r: 56, g: 131, b: 168, a: 0.5}
```
```javascript
rc.stringifyRGB( rc.HSLAtoRGB("hsla(100, 50%, 50%, 1)") );
//"rgb(106, 191, 64)"
```

[🔝](#documentation)


### HSLAtoRGB
- converts HSLA color to RGB
- input: object {h: hue, s: saturation, l: lightness, a: alpha}
	- or HSLA string
- returns: object `{r: red, g: green, b: blue, a: alpha}`

```javascript
rc.HSLAtoRGB({h: 0, s: 90, l: 55})
//{r: 244, g: 37, b: 37, a: 1}
```

[🔝](#documentation)


### RGBtoHEX
- converts RGB color to hexadecimal
- input: object `{r: red, g: green, b: blue, a: alpha}`
	- or string with RGB color
- returns: string with hexadecimal color

```javascript
rc.RGBtoHEX({r: 245, g: 172, b: 36})
//"#f5ac24"
```
```javascript
rc.RGBtoHEX("rgba(245, 172, 36)")
//"#f5ac24"
```

[🔝](#documentation)


### HSLAtoHEX
- converts HSLA to hexadecimal
- input: object `{h: hue, s: saturation, l: lightness, a: alpha}`
	- or string with HSLA color
- returns: string with hexadecimal color

```javascript
rc.RGBtoHEX({r: 245, g: 172, b: 36})
//"#f5ac24"
```
```javascript
rc.HSLAtoHEX("hsla(39, 91%, 55%, 1)")
//"#f5ac24"
```

[🔝](#documentation)


